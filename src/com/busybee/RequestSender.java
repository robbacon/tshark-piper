package com.busybee;

import com.busybee.models.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import com.google.gson.Gson;

public class RequestSender {
    private String url;
    private final Gson GSON = new Gson();
    private static final String post = "POST";
    private static final String get = "GET";

    public RequestSender(Properties properties) throws MalformedURLException {
        url = properties.getProperty("server.url");
    }

    public void postCount(CountWithKey countToSend) throws IOException {
        String path = "/count/submit";
        String response = sendRequestObject(path, post, countToSend);

        if(response == null) {
            System.out.println("Incorrect key");
        }
    }

    public Node postDevice(Node nodeToSend) throws IOException {
        String path = "/node/verify";

        String response = sendRequestObject(path, post, nodeToSend);
        Node node = null;
        if(response.equals("")) {
            System.out.println("Couldn't confirm identity");
            System.exit(0);
        } else {
            node = GSON.fromJson(response , Node.class);
        }
        return node;
    }

    public Node postupdateNodeLocation(String nodeId, LocationWithKey location) throws IOException {
        String path = "/node/" + nodeId + "/location";

        String response = sendRequestObject(path, post, location);
        Node node = null;
        if(response.equals("")) {
            System.out.println("Couldn't confirm identity");
            System.exit(0);
        } else {
            node = GSON.fromJson(response , Node.class);
        }
        return node;
    }

    public Location getLocation(long id) throws IOException {
        String path = "/location/retrieve/" + id;
        String response = sendRequest(path, get);
        Location location = null;
        if(response.equals("")) {
            System.out.println("Couldn't confirm identity");
            System.exit(0);
        } else {
            location = GSON.fromJson(response , Location.class);
        }
        return location;
    }

    public String getKey(Node node) throws IOException {
        String path = "/node/login";
        String response = sendRequestObject(path, get, node);
        if(response.equals("")) {
            System.out.println("Couldn't confirm identity");
            System.exit(0);
        }
        return response;
    }

    public String sendRequest(String urlPath, String requestType) throws IOException {
        return sendRequestObject(urlPath, requestType, null);
    }

    public String sendRequestObject(String urlPath, String requestType, Object objectToSend) throws IOException {
        URL api = new URL( url + urlPath);
        HttpURLConnection con = (HttpURLConnection) api.openConnection();
        con.setRequestMethod(requestType);
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);

        if(objectToSend != null) {
            String jsonInputString = GSON.toJson(objectToSend);
            // Open output stream to send an object
            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            } catch (Exception e) {
                System.out.println(String.format("Server returned message: %s", e.getMessage()));
                System.exit(0);
            }
        }

        // Receive response
        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(String.format("Server returned object: %s", response.toString()));

            return response.toString();
        }  catch (Exception e) {
            System.out.println(String.format("Server returned message: %s", e.getMessage()));
            System.exit(0);
            return null;
        }
    }
}
