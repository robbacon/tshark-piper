package com.busybee.models;

public class Location {
    private long id;
    private String headerName;
    private String subHeaderName;
    private double latitude;
    private double longitude;

    public Location() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getSubHeaderName() {
        return subHeaderName;
    }

    public void setSubHeaderName(String subHeaderName) {
        this.subHeaderName = subHeaderName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
