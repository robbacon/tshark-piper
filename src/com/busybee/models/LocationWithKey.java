package com.busybee.models;

public class LocationWithKey {
    Location location;
    String key;

    public LocationWithKey() {}

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
