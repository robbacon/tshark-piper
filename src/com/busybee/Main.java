package com.busybee;

import com.busybee.models.*;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        DataParser dataParser = new DataParser();

        // Loading jar stored properties file
        FileInputStream file;
        String path = "./application.properties";
        File f = new File(path);
        Properties props = new Properties();
        if (!f.exists()) {
            try {
                props.load(Main.class.getResourceAsStream("/application.properties"));
            } catch (FileNotFoundException e) {
                System.out.println(String.format("[ERROR] File not found", e));
            } catch (IOException e) {
                System.out.println(String.format("[ERROR] Error reading properties", e));
            }
        } else {
            file = new FileInputStream(path);
            props.load(file);
            file.close();
        }

        String nodeId = props.getProperty("device.id");
        String devicePassword = props.getProperty("device.password");

        System.out.println("Started");
        RequestSender requestSender = new RequestSender(props);

        Node node = new Node();
        String key = "";
        String password = "";

        if (nodeId != null) {
            node.setId(Long.parseLong(nodeId));
            node.setPassword(devicePassword);

            key = requestSender.getKey(node);
            if (args.length == 0) {
                node = requestSender.postDevice(node);
            } else if (args.length == 1 && Long.parseLong(args[0]) > 0) {
                // Update node with existing location
                Location location = requestSender.getLocation(Long.parseLong(args[0]));
                LocationWithKey loc = new LocationWithKey();
                loc.setKey(key);
                loc.setLocation(location);

                node = requestSender.postupdateNodeLocation(nodeId, loc);
            } else if (args.length == 4) {
                // Update node with new location
                Location location = new Location();
                location.setHeaderName(args[0]);
                location.setSubHeaderName(args[1]);
                location.setLatitude(Double.valueOf(args[2]));
                location.setLongitude(Double.valueOf(args[3]));

                LocationWithKey loc = new LocationWithKey();
                loc.setKey(key);
                loc.setLocation(location);

                node = requestSender.postupdateNodeLocation(nodeId, loc);

            }
        } else if (args.length == 1 && args[0].matches("\\d*") && Long.parseLong(args[0]) > 0) {
            // Register node and use existing location
            Location location = requestSender.getLocation(Long.parseLong(args[0]));

            node.setLocation(location);
            password = generatePassword();
            node.setPassword(password);
            node = requestSender.postDevice(node);
            node.setPassword(password);

            key = requestSender.getKey(node);
        } else if (args.length == 4) {
            // Register node and create new location
            Location location = new Location();
            location.setHeaderName(args[0]);
            location.setSubHeaderName(args[1]);
            location.setLatitude(Double.valueOf(args[2]));
            location.setLongitude(Double.valueOf(args[3]));

            node.setLocation(location);
            password = generatePassword();
            node.setPassword(password);
            node = requestSender.postDevice(node);
            node.setPassword(password);

            key = requestSender.getKey(node);
        } else {
            System.out.println("No node found, no location provided!");
            System.exit(0);
        }
        props.setProperty("device.id", String.valueOf(node.getId()));
        if(!password.equals("")) {
            props.setProperty("device.password", password);
        }
        System.out.println("Key Generated: " + key);

        props.store(new FileOutputStream("./application.properties"), null);

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s;
        Map<String, Long> uniqueDevices = new HashMap<>();

        // Set the current hour to compare against incoming packets
        int curHour = dataParser.getHourFromEpoch(String.valueOf(Instant.now().getEpochSecond()));

        // Loop through the constant packet stream
        while ((s = in.readLine()) != null && s.length() != 0) {
            String[] split = s.split("\\s+");
            String timeTaken = split[0];
            // Packets sometimes only contain a time from epoch and contain no unique identifier
            if (split.length > 1 && !uniqueDevices.containsKey(dataParser.hash(split))) {
                String macAddress = split[1];
                // Sometimes MAC address can be blank
                if (macAddress.equals("")) {
                    System.out.println("Empty MAC address \n");
                } else {
                    System.out.println(String.format("Unique MAC Address \n"));
                    String hashedIP = dataParser.hash(split);
                    long epoch = Long.parseLong(timeTaken.split("\\.")[0]);
                    uniqueDevices.put(hashedIP, epoch);
                }
            }

            if (dataParser.getHourFromEpoch(timeTaken) != curHour) {
                System.out.println("Submitting");

                // Aggregating devices for count
                Count newCount = new Count();
                newCount.setAmount(uniqueDevices.size());
                newCount.setLocation(node.getLocation());

                LocalDateTime ldt = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
                ldt = ldt.minusHours(1).truncatedTo(ChronoUnit.HOURS);

                long secondsSinceEpoch = ldt.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
                newCount.setTimeTaken(secondsSinceEpoch);

                CountWithKey c = new CountWithKey();
                c.setCount(newCount);
                c.setKey(key);

                System.out.println("Submitting Count \n");
                requestSender.postCount(c);
                uniqueDevices.clear();
                // Updating for current hour
                curHour = dataParser.getHourFromEpoch(timeTaken.split("\\.")[0]);
            }
        }
    }

    // Generate password that nodes use generate a key and communicate with the backend
    private static String generatePassword() {
        return new Random().ints(10, 33, 122).collect(StringBuilder::new,
                StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}

