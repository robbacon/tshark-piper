package com.busybee;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DataParser {

    // Will add a salt to string and hash it for anonymity
    public String hash(String[] stringsToEncrypt) throws NoSuchAlgorithmException {
        String[] correctEpoch = stringsToEncrypt[0].split("\\.");
        // Using date as salt
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(Long.parseLong(correctEpoch[0]), 0, ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/y h", Locale.ENGLISH);
        String salt = dateTime.format(formatter);
        String stringToEncrypt = stringsToEncrypt[1] + salt;

        // Running hash on string
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(stringToEncrypt.getBytes());
        return new String(messageDigest.digest());
    }

    public int getHourFromEpoch(String epoch) {
        String[] correctEpoch = epoch.split("\\.");
        long epochFormatted = Long.parseLong(correctEpoch[0]);

        LocalTime timeOfDay = Instant.ofEpochSecond(epochFormatted)
                .atOffset(ZoneOffset.UTC)
                .toLocalTime();
        return timeOfDay.getHour();
    }

}
