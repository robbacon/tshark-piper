#!/bin/bash


Help()
{
   # Display Help
   echo "Capture devices for location. Register a new location through these arguments"
   echo
   echo "Syntax: sudo ./start.sh [-I|L|H|S|A|O|h]"
   echo "options:"
   echo "I {Listing ID}   Assign node to existing location."
   echo "L                    Create a location for this node: requires -H|SH|LA|LO"
   echo "H   \"{Header}\"       New Location Header"
   echo "S  \"{Sub Header}\"   New Location Sub Header"
   echo "A  \"{Latitude}\"     New Location Latitude"
   echo "O  \"{Longitude}\"    New Location Longitude"
   echo "h                   Print this Help."
   echo
}

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
lcreate=0
header=""
subheader=""
latitude=""
longitude=""
lid=0

while getopts "hLH:S:A:O:I:" option; do
   case $option in
      h) # display Help
         Help
         exit;;
      L) lcreate=1
         if [[ $# -ne 9 ]]; then
	  echo "Incorrect Parameters, check -h"
          exit
	 fi
        ;;
      H) if [[ $lcreate -eq 1 && $# -eq 9 ]]; then
          header=$OPTARG
        else
          echo "Incorrect Parameters, check -h"
          exit
        fi
        ;;
      S) if [[ $lcreate -eq 1 && $# -eq 9 ]]; then
          subheader=$OPTARG
        else
          echo "Incorrect Parameters, check -h"
          exit
        fi
        ;;
      A) if [[ $lcreate -eq 1 && $# -eq 9 ]]; then
          latitude=$OPTARG
         else
          echo "Incorrect Parameters, check -h"
          exit
        fi
        ;;
      O) if [[ $lcreate -eq 1 && $# -eq 9 ]]; then
          longitude=$OPTARG
        else
          echo "Incorrect Parameters, check -h"
          exit
          fi
        ;;
      I) if [[ $# -eq 2 ]]; then
            lid=$OPTARG
          else
            echo "-I can only be passed on it's own"
          exit
        fi
        ;;
     \?) # incorrect option
         echo "Error: Invalid option"
         exit;;
   esac
done

echo "Checking Libraries"

apt-get update -y
apt-get install openjdk-8-jdk -y
apt-get install tshark -y

if [[ $lid -ne 0 ]]; then
  sudo tshark -l -I -i wlan1 -t e -T fields -e frame.time_epoch -e wlan.sa | java -jar tshark-piper.jar "$lid"

elif [[ ! -z $header && ! -z $subheader && ! -z $latitude && ! -z $longitude ]]; then
  sudo tshark -l -I -i wlan1 -t e -T fields -e frame.time_epoch -e wlan.sa | java -jar tshark-piper.jar "$header" "$subheader" "$latitude" "$longitude"

else
  sudo tshark -l -I -i wlan1 -t e -T fields -e frame.time_epoch -e wlan.sa | java -jar tshark-piper.jar
fi